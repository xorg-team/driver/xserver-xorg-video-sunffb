xserver-xorg-video-sunffb (1:1.2.2-1) unstable; urgency=low

  * New upstream release.
  * Disable silent build rules.

 -- Julien Cristau <jcristau@debian.org>  Sun, 29 Sep 2013 14:13:03 +0200

xserver-xorg-video-sunffb (1:1.2.1-4) unstable; urgency=low

  * Switch to dh:
    - Use debhelper 8.
    - Use dh-autoreconf.
    - Bump xserver-xorg-dev build-dep for dh_xsf_substvars and xsf
      debhelper sequence.
  * Remove xsfbs accordingly.
  * Update Uploaders list. Thanks, David!
  * Remove long obsolete Replaces/Conflicts.
  * Wrap Depends/Provides.
  * Bump Standards-Version to 3.9.1 (no changes needed).

 -- Cyril Brulebois <kibi@debian.org>  Sat, 05 Feb 2011 14:57:46 +0100

xserver-xorg-video-sunffb (1:1.2.1-3) experimental; urgency=low

  * Build against Xserver 1.9.1 rc1.
  * Add myself to Uploaders.

 -- Cyril Brulebois <kibi@debian.org>  Sat, 16 Oct 2010 22:32:55 +0200

xserver-xorg-video-sunffb (1:1.2.1-2) unstable; urgency=low

  * Rename the build directory to not include DEB_BUILD_GNU_TYPE for no
    good reason.  Thanks, Colin Watson!
  * Remove myself from Uploaders
  * Update xsfbs, use new ${xviddriver:Depends} substvar.

 -- Julien Cristau <jcristau@debian.org>  Sat, 05 Jun 2010 15:12:49 +0200

xserver-xorg-video-sunffb (1:1.2.1-1) unstable; urgency=low

  [ Timo Aaltonen ]
  * New upstream release.
  * Bump Standards-Version to 3.8.3.
  * Build against Xserver 1.7.

 -- Julien Cristau <jcristau@debian.org>  Fri, 15 Jan 2010 13:57:01 +0000

xserver-xorg-video-sunffb (1:1.2.0-2) unstable; urgency=low

  * Add missing comma in Build-Depends (LP: #391562).  Thanks, Bryce
    Harrington!
  * Add README.source.  Bump Standards-Version to 3.8.2.
  * Cherry-pick patch from git master to remove libdrm requirement from
    configure.ac, as DRI support has been removed.

 -- Julien Cristau <jcristau@debian.org>  Sat, 27 Jun 2009 19:17:27 +0200

xserver-xorg-video-sunffb (1:1.2.0-1) unstable; urgency=low

  * New upstream release.
  * Run autoreconf on build.
  * Allow builds with parallel=% in DEB_BUILD_OPTIONS.
  * Remove patch ffb_screeninitsegv.diff, included upstream.
  * Remove libdrm-dev and libgl-dev build-deps, DRI support has been removed.

 -- Julien Cristau <jcristau@debian.org>  Mon, 04 May 2009 18:11:04 +0200

xserver-xorg-video-sunffb (1:1.1.0-4) unstable; urgency=low

  * Add patch from Bernhard R. Link to fix segfault on server regen
    (closes: #455313).
  * Bump Standards-Version to 3.7.3.
  * Drop the XS- prefix from Vcs-* control fields.
  * Run dpkg-shlibdeps with --warnings=6.  Drivers reference symbols from
    /usr/bin/Xorg and other modules, and that's not a bug, so we want
    dpkg-shlibdeps to shut up about symbols it can't find.  Build-depend on
    dpkg-dev >= 1.14.17.
  * xsfbs.mk: serverabi now depends on install, which fixes the build with
    -j2 (closes: #471193).  Thanks, Bernhard R. Link!
  * Also fix some target dependencies in debian/rules, again thanks to
    Bernhard.

 -- Julien Cristau <jcristau@debian.org>  Sun, 01 Jun 2008 14:50:56 +0200

xserver-xorg-video-sunffb (1:1.1.0-3) unstable; urgency=low

  [ David Nusinow ]
  * Generate server dependencies automatically from the ABI

  [ Julien Cristau ]
  * Add link to xserver-xorg-core bug script, so that bugreports contain
    the user's config and log files.
  * Add myself to uploaders, and remove Branden with his permission.
  * Rebuild against xserver 1.4.

  [ Brice Goglin ]
  * Pull upstream manpage fix b86a3f4662d384e3a3540340bfd5171ab2523c34
  * Install the upstream changelog.
  * Generate Provides: line automatically.
  * Bump Build-Depends: xserver-xorg-dev to >= 2:1.2.99.902
    (needed to let xsfbs get access to serverminver).
  * Add XS-Vcs-*.
  * Add a link to www.X.org and a reference to the xf86-video-sunffb
    module in the long description.
  * Remove Fabio from uploaders with his permission. He's always welcome back.
  * Add upstream URL to debian/copyright.

  [ Timo Aaltonen ]
  * Replaces/Conflicts: xserver-xorg-driver-sunffb.

 -- Julien Cristau <jcristau@debian.org>  Tue, 25 Sep 2007 10:31:33 +0200

xserver-xorg-video-sunffb (1:1.1.0-2) unstable; urgency=low

  * Bump the version of xserver-xorg-core dependency to (>= 2:1.1.1-1).
  * There is now a watch file (debian/watch), thanks to Thierry Reding.

 -- Jurij Smakov <jurij@debian.org>  Thu,  5 Oct 2006 20:29:08 -0700

xserver-xorg-video-sunffb (1:1.1.0-1) unstable; urgency=low
  
  * New upstream release.
  * Updates for transition to X.org 7.1:
    - make build-dependency on xserver-xorg-dev versioned (>= 2:1.1.1)
    - we now provide xserver-xorg-video-1.0
  * Miscellaneous updates:
    - bump compat level to 5, adjust build-dep on debhelper accordingly
    - update and expand copyright file

 -- Jurij Smakov <jurij@debian.org>  Tue, 19 Sep 2006 22:17:56 -0700

xserver-xorg-video-sunffb (1:1.0.1.3-2) unstable; urgency=low

  * First upload to unstable.
  * Add myself to uploaders.
  * Bump policy version to 3.7.2.0, no changes neccessary.
  * Update the copyright file.

 -- Jurij Smakov <jurij@debian.org>  Sat, 24 Jun 2006 21:23:27 -0700

xserver-xorg-video-sunffb (1:1.0.1.3-1) experimental; urgency=low

  * First upload to Debian
  * Change source package, package, and provides names to denote the
    type of driver and that they are for xserver-xorg
  * Port patches from trunk
    + sparc/105_ffb_dac_typedef_bool.diff

 -- David Nusinow <dnusinow@debian.org>  Sun,  5 Mar 2006 20:28:40 -0500

xserver-xorg-driver-sunffb (1:1.0.1.3-0ubuntu1) dapper; urgency=low

  * New upstream release.
  * Change libdrm2 B-D to libdrm-dev.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Wed,  4 Jan 2006 16:18:01 +1100

xserver-xorg-driver-sunffb (1:1.0.1.2-0ubuntu2) dapper; urgency=low

  * Build-Dep on libdrm2 instead of libdrm.

  * Add Build-Dep on x11proto-gl-dev.

 -- Fabio M. Di Nitto <fabbione@ubuntu.com>  Tue, 27 Dec 2005 06:26:05 +0100

xserver-xorg-driver-sunffb (1:1.0.1.2-0ubuntu1) dapper; urgency=low

  * First xserver-xorg-driver-sunffb release.

 -- Daniel Stone <daniel.stone@ubuntu.com>  Mon, 19 Dec 2005 09:12:09 +1100
